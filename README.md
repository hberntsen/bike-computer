# DIY Bluetooth heart rate & cadence bike computer

This repository contains the source files for my custom bike computer. It
sets up two Bluetooth connections, one to a heart rate monitor and another to a
cadence sensor.

This repository includes code from:
- [Zephyr](https://www.zephyrproject.org/) (operating system)
- [u8g2](https://github.com/olikraus/u8g2) (graphics library)

It is based on
- An [Eastwin NRF52832 module](https://web.archive.org/web/20191102171412/https://www.alibaba.com/product-detail/Bluetooth-Low-Energy-Module-Nordic-NRF52832_60667080247.html).
- A Nokia 3310 screen (PCD8544). The Zephyr driver for it is integrated in the
  source code. If anyone wants to help me with upstreaming this, send me a
  message!
- A CR2032 battery compartment including power switch from an LED tea candle light
- A custom 3D-printed enclosure (see scad folder)

The application code resides in the `zephyr/samples/bluetooth/harms_hrm` folder.
