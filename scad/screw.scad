// use 2.4 thread for vertical printing, 2.2 for horizontal
module screw(height, head=3.6, cone=1, thread=2.4) {
    $fa = 1;
    $fs = 1;

    cylinder(h=cone, r1=head / 2, r2=thread/2);
    cylinder(h=height, d=thread);
}
