
// from displaytest.scad
display_wall_thickness= 0.8 * 9/6;
display_inner_size = 40.5;
display_outer_size = display_inner_size + 2 * display_wall_thickness;

wall_thickness=0.8;
margin = 0.1;
xy_size = display_outer_size+2*wall_thickness+2*margin;
border_size = 3;

module bottom () {
    open_size = xy_size-border_size*2;
    offset = (xy_size - open_size) / 2;
    difference() {
        square([xy_size, xy_size]);
        translate([offset, offset])
            #square([open_size, open_size]);
    };
}

module sides() {
    square([xy_size, wall_thickness]);
    square([wall_thickness, xy_size]);
    translate([xy_size - wall_thickness, 0])
        square([wall_thickness, xy_size]);
    translate([0, xy_size - wall_thickness])
        square([xy_size, wall_thickness]);
}

linear_extrude(wall_thickness)
    bottom();
linear_extrude(border_size*2)
    sides();
