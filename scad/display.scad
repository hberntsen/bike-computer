include <screw.scad>;

display_x=39;
display_y=36;
display_z=4;

display_left_top_right_margin = 3;
display_window_x = display_x - 2 * display_left_top_right_margin;
display_window_y = display_y - 3 - 9;

floor_thickness = 0.8;
wall_thickness= 0.8 * 9/6;
screen_wall_thickness_x = 1.9;
screen_wall_thickness_y = 2.27;

screen_holder_x = screen_wall_thickness_x + display_x + screen_wall_thickness_x;
screen_holder_y = screen_wall_thickness_y + display_y + screen_wall_thickness_y;
screen_holder_z = 5;

// this is battery_part_x in steermount
inner_size = 40.5;
outer_size = inner_size + 2 * wall_thickness;

// includes battery compartiment
inner_components = 20;
total_z = screen_holder_z + inner_components;

// renders screws with the Z offset being how far it is removed from the bottom/top of the steer mount
module screws() {
    // variables from steermount: 
    steering_diameter = 25;
    steering_wheel_surface_x = 8;
    steering_wheel_bars_y = 8;
    steering_wheel_bars_front_z = 3;

    battery_part_x = 40;
    battery_part_y = battery_part_x;
    battery_part_z_safety = 4;
    steering_wheel_inclusion = steering_diameter / 5;

    // START from battery.scad
    battery_lower_diameter = 36;

    // END from battery.scad
    battery_margin_ring_diameter = battery_lower_diameter + 1;

    total_z = steering_wheel_inclusion + battery_part_z_safety;
    screw_y = 5;
    screw_thread = 2.2;
    contact_plate_x = 4.1;
    contact_plate_y = 0.8;
    // end variables from steermount


    // We only need the hole since the head will be in another part
    y_offset = 5;
    z_offset = - total_z / 2;
    // front screws
    translate([steering_wheel_surface_x / 2, battery_part_y / 2 + y_offset, z_offset]) {
        rotate([90, 0, 0])
            screw(screw_y + y_offset, thread=screw_thread);
    }

    translate([battery_part_x - steering_wheel_surface_x / 2 , battery_part_y / 2 + y_offset, z_offset]) {
        rotate([90, 0, 0])
            screw(screw_y + y_offset, thread=screw_thread);
    }

    //back screws
    translate([steering_wheel_surface_x / 2, -battery_part_y / 2 - y_offset, z_offset]) {
        rotate([270, 0, 0])
            screw(screw_y + y_offset, thread=screw_thread);
    }

    translate([battery_part_x - steering_wheel_surface_x / 2 , -battery_part_y / 2 - y_offset, z_offset]) {
        rotate([270, 0, 0])
            screw(screw_y + y_offset, thread=screw_thread);
    }
}

// end shared

screw_y = 5;

module screen_holder() {
    // floor/window for screen
    linear_extrude(floor_thickness) {
        difference() {
            square([screen_holder_x, screen_holder_y]);
            translate([(screen_holder_x - display_window_x) / 2, screen_wall_thickness_y + display_left_top_right_margin])
                square([display_window_x, display_window_y]);
        };
    }

    //sides for screen
    difference() {
        cube([screen_holder_x, screen_holder_y, screen_holder_z]);
        translate([screen_wall_thickness_x, screen_wall_thickness_y])
            cube([display_x, display_y, screen_holder_z * 2]);

    }
}

module steer_spacer() {
    // from steermount.scad
    steering_diameter = 25;
    steering_wheel_surface_x = 8;

    steering_wheel_bars_front_z = 3;
    battery_part_z_safety = 4;
    steering_wheel_inclusion = steering_diameter / 5;
    _total_z = steering_wheel_inclusion + battery_part_z_safety;
    // end from steermount.scad

    x_size = inner_size - steering_wheel_surface_x * 2;
    z_size = _total_z - steering_wheel_bars_front_z;

    //spacer for switch
    translate([wall_thickness + steering_wheel_surface_x, -0.01, total_z - z_size]) {
        cube([x_size, 10,  z_size]);
    }

    // steering bar
    translate([-100, outer_size / 2, steering_diameter + steering_diameter / 2 - steering_wheel_inclusion])
        rotate([0, 90, 0])
            cylinder(d=steering_diameter, h=500);
}

/// implements https://eleccelerator.com/prevent-corner-lifting-3d-printing-overkill-method/
module base(inner_x, inner_y, z, spacing = 0.8, base_z = 0.3) {
    base_x = inner_x + 2 * z + 2 * spacing;
    base_y = inner_y + 2 * z + 2 * spacing;

    translate([-z -spacing, -z -spacing, 0 ]) {
        translate([0, 0, -base_z]) 
            linear_extrude(base_z)
                difference() {
                    square([base_x, base_y]);
                    translate([z + spacing , z+ spacing ])
                        square([inner_x , inner_y ]);
                }

        translate([z, 0, 0])
            rotate([0, 90, 90])
                linear_extrude(base_x)
                    polygon(points=[[0,0], [z, 0], [0, z]]);

        translate([inner_x + z*2 + 2* spacing, z, 0])
            rotate([0, 90, 180])
                linear_extrude(base_x)
                    polygon(points=[[0,0], [z, 0], [0, z]]);

        translate([0, 2*spacing + inner_y + z, 0])
            rotate([0, 90, 0])
                linear_extrude(base_x)
                    polygon(points=[[0,0], [z, 0], [0, z]]);

        translate([2* spacing + inner_y + z, 2* spacing + 2* z + inner_y , 0])
            rotate([90, 90, 0])
                linear_extrude(base_x)
                    polygon(points=[[0,0], [z, 0], [0, z]]);
    }
}


difference() {
union() {
    translate([(outer_size - screen_holder_x) / 2, (outer_size - screen_holder_y) / 2, 0])
        screen_holder();

    linear_extrude(total_z) {
        difference() {
            square([outer_size, outer_size]);
            translate([wall_thickness, wall_thickness])
                square([inner_size, inner_size]);
        };
    }
};

union() {
steer_spacer();
translate([wall_thickness, outer_size / 2, total_z])
    screws();
}; 
};

translate([outer_size, 0, 0])
    rotate([0,180,0])
        base(outer_size, outer_size, 8, 0.8,  0.2);
