include <screw.scad>;

steering_diameter = 25;
steering_wheel_surface_x = 8;
steering_wheel_bars_y = 8;
steering_wheel_bars_front_z = 3;

battery_part_x = 40;
battery_part_y = battery_part_x;
battery_part_z_safety = 4;
steering_wheel_inclusion = steering_diameter / 5;

// START from battery.scad
battery_lower_diameter = 36;

// END from battery.scad
battery_margin_ring_diameter = battery_lower_diameter + 1;

total_z = steering_wheel_inclusion + battery_part_z_safety;
screw_y = 5;
screw_thread = 2.2;
contact_plate_x = 4.1;
contact_plate_y = 0.8;

module steering_bar() {
    translate([-100, 0,  steering_wheel_inclusion - steering_diameter / 2])
    rotate([0, 90, 0])
        cylinder(d=steering_diameter, h=500);
}

/*
module switch_bar() {
    translate([battery_part_x / 2 , 0,  steering_wheel_inclusion - steering_diameter / 2 + 2])
    rotate([0, 90, 90])
        cylinder(d=steering_diameter, h=50);
}
*/

module steering_mount() {
    translate([0, -battery_part_x/2, 0])
        cube([steering_wheel_surface_x, battery_part_y, total_z]);

    translate([battery_part_x - steering_wheel_surface_x, -battery_part_x/2, 0])
        #cube([steering_wheel_surface_x, battery_part_y, total_z]);

    translate([0, -battery_part_x/2, 0])
        cube([battery_part_x, steering_wheel_bars_y, total_z]);

    translate([0, battery_part_x/2 - steering_wheel_bars_y, steering_wheel_inclusion + battery_part_z_safety - steering_wheel_bars_front_z ])
        #cube([battery_part_x, steering_wheel_bars_y,  steering_wheel_bars_front_z]);
}

module battery_part() {
    battery_lower_diameter = 36.2;
    battery_lower_z = 40;

    battery_upper_diameter = 35;
    battery_upper_z = 1;

    translate([(battery_part_x - battery_lower_diameter)/2 + battery_lower_diameter / 2, 0, -battery_upper_z]) {
        rotate([0, 180, 0])
        cylinder(h = battery_lower_z, d = battery_lower_diameter);
        cylinder(h = battery_upper_z, d = battery_upper_diameter);
    }
}

module screws() {
    // We only need the hole since the head will be in another part
    y_offset = 5;
    z_offset = total_z / 2;
    // front screws
    translate([steering_wheel_surface_x / 2, battery_part_y / 2 + y_offset, z_offset]) {
        rotate([90, 0, 0])
            screw(screw_y + y_offset, thread=screw_thread);
        translate([-contact_plate_x / 2, -y_offset - screw_y, -screw_thread/2 ])
            cube([contact_plate_x, contact_plate_y, 10]);
    }

    translate([battery_part_x - steering_wheel_surface_x / 2 , battery_part_y / 2 + y_offset, z_offset]) {
        rotate([90, 0, 0])
            screw(screw_y + y_offset, thread=screw_thread);
        translate([-contact_plate_x / 2, -y_offset - screw_y, -screw_thread/2 ])
            cube([contact_plate_x, contact_plate_y, 10]);
    }

    //back screws
    translate([steering_wheel_surface_x / 2, -battery_part_y / 2 - y_offset, z_offset]) {
        rotate([270, 0, 0])
            screw(screw_y + y_offset, thread=screw_thread);
        translate([-contact_plate_x / 2, y_offset + screw_y, -screw_thread/2 ])
            cube([contact_plate_x, contact_plate_y, 10]);
    }

    translate([battery_part_x - steering_wheel_surface_x / 2 , -battery_part_y / 2 - y_offset, z_offset]) {
        rotate([270, 0, 0])
            screw(screw_y + y_offset, thread=screw_thread);
        translate([-contact_plate_x / 2, y_offset + screw_y, -screw_thread/2 ])
            cube([contact_plate_x, contact_plate_y, 10]);
    }
}

/// implements https://eleccelerator.com/prevent-corner-lifting-3d-printing-overkill-method/
module base(inner_x, inner_y, z, spacing = 0.8, base_z = 0.3) {
    base_x = inner_x + 2 * z + 2 * spacing;
    base_y = inner_y + 2 * z + 2 * spacing;

    translate([-z -spacing, -z -spacing, -base_z]) {
        linear_extrude(base_z)
            difference() {
                square([base_x, base_y]);
                translate([z + spacing , z+ spacing ])
                    square([inner_x , inner_y ]);
            }

        translate([z, 0, 0])
            rotate([0, 90, 90])
                linear_extrude(base_x)
                    polygon(points=[[0,0], [z, 0], [0, z]]);

        translate([inner_x + z*2 + 2* spacing, z, 0])
            rotate([0, 90, 180])
                linear_extrude(base_x)
                    polygon(points=[[0,0], [z, 0], [0, z]]);

        translate([0, 2*spacing + inner_y + z, 0])
            rotate([0, 90, 0])
                linear_extrude(base_x)
                    polygon(points=[[0,0], [z, 0], [0, z]]);

        translate([2* spacing + inner_y + z, 2* spacing + 2* z + inner_y , 0])
            rotate([90, 90, 0])
                linear_extrude(base_x)
                    polygon(points=[[0,0], [z, 0], [0, z]]);
    }
}

union() {
    translate([0, -battery_part_x/2, total_z])
        base(battery_part_x, battery_part_y, total_z / 2);

    difference() {
        steering_mount();
        union() {
            steering_bar();
            translate([0, 0, total_z+0.01])
                battery_part();
            screws();
        };
    }
};
