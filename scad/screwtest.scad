screw_head = 3.6;
screw_cone = 1;
screw_thread = 1.4;
screw_height = 15;

$fa = 1;
$fs = 1;


module screw(thread_diameter) {
    cylinder(h=screw_cone, r1=screw_head / 2, r2=screw_thread/2);
    cylinder(h=screw_height, d=thread_diameter);
}


module wrap() {
            cube([10, 25, 10]);
            translate([3, 1, -1])
                linear_extrude(2)
                    rotate([0, 180, 0])
                        text("2", 4);
}

difference() {
    translate([-5, -5, 0.01])
        wrap();
    {
        screw(2);
        translate([0, 5, 0])
            screw(2.2);
        translate([0, 10, 0])
            screw(1.8);
        translate([0, 15, 0])
            screw(2.4);
    };
}
