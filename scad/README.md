# OpenSCAD models

- `display.scad` is the top part that will hold the PCD8544 display. Is is designed to clamp the display module. To ensure the display stays there I've put a piece of foam behind the display module. I've protected the display itself by putting a piece of transparent, sticky book-protector stuff on top of it as screen protector.
- `display_protector.scad` is a small cover to ensure that rain will not go between the case and the screen protector sticker.
- `steermount.scad` is the 'base'. It precisely fits my candle-battery holder. The display part is slid on top of this and is held by the 4 screws. There are 4 holes on top, I've put a copper plate in each hole to expose the programming pins via the screws. The screws also function as support for a rubber band that mounts the device to my bike's steering wheel.
