#pragma once

struct device;

int pcd8544_write_from(const struct device *dev, const u16_t x, const u16_t row, const u16_t len, unsigned char *buf);
int pcd8544_write_u8g2(const struct device * dev, unsigned char *buf);
