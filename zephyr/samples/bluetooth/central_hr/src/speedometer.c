#include <zephyr/types.h>
#include <stddef.h>
#include <errno.h>
#include <zephyr.h>
#include <sys/printk.h>

#include <drivers/counter.h>
#include <device.h>
#include <drivers/gpio.h>

#define DELAY (1000 * 1000)
#define ALARM_CHANNEL_ID 0
static struct counter_alarm_cfg speedometer_alarm_cfg;

static struct device *gpio_device;
static const uint32_t led_pin = 12; // D7 (11)
static const uint32_t speedometer_pin = 11; // D7 (11)

uint32_t speedometer_desired_speed = 42;

static void test_counter_interrupt_fn(struct device *counter_dev,
				u8_t chan_id, u32_t ticks, void *user_data)
{
    int err = 0;

    if(user_data == 0) {
        int err = gpio_pin_configure(gpio_device, speedometer_pin, GPIO_DIR_OUT);
        err |= gpio_pin_write(gpio_device, speedometer_pin, 0);
        err |= gpio_pin_write(gpio_device, led_pin, 1);
        if(err) {
                printk("Interrupt: pin configure error: %d\n", err);
                return;
        }
        speedometer_alarm_cfg.ticks = counter_us_to_ticks(counter_dev, 2 * 1000);
        speedometer_alarm_cfg.user_data = (void*)1;
        counter_set_channel_alarm(counter_dev, ALARM_CHANNEL_ID, &speedometer_alarm_cfg);
    } else {
        err = gpio_pin_configure(gpio_device, speedometer_pin, GPIO_DIR_IN);
        err |= gpio_pin_write(gpio_device, led_pin, 0);
        if(err) {
                printk("Interrupt: pin configure 2 error: %d\n", err);
                return;
        }
        // 576 == 7
        // 300 == 13.5
        // 100 == 37.8
        
        // 192-5 = 21.5
        // 90-5 = 46.2
        // 50-5 = 83
        // 125 = 31.5
        // 60 = 63.5
        // 250 = 16
        // 500 = 8
        // 750 = 5.5
        // 1000 = 4

        if(speedometer_desired_speed == 0) {
            speedometer_alarm_cfg.ticks = counter_us_to_ticks(counter_dev, 250 * 1000);
            speedometer_alarm_cfg.user_data = (void*)1;
        } else {
            speedometer_alarm_cfg.ticks = counter_us_to_ticks(counter_dev, (3962500/speedometer_desired_speed - 2000 ));
            speedometer_alarm_cfg.user_data = (void*)0;
        }
        counter_set_channel_alarm(counter_dev, ALARM_CHANNEL_ID, &speedometer_alarm_cfg);
    }
}

bool counter_init() {
	struct device *counter_dev;
	int err = 0;

	counter_dev = device_get_binding(DT_RTC_2_NAME);
	if (counter_dev == NULL) {
		printk("Counter device not found\n");
		return false;
	}

	counter_start(counter_dev);

	speedometer_alarm_cfg.flags = 0;
	speedometer_alarm_cfg.ticks = counter_us_to_ticks(counter_dev, DELAY);
	speedometer_alarm_cfg.callback = test_counter_interrupt_fn;
	speedometer_alarm_cfg.user_data = 0;

	err = counter_set_channel_alarm(counter_dev, ALARM_CHANNEL_ID,
					&speedometer_alarm_cfg);
	printk("Set alarm in %d sec\n", speedometer_alarm_cfg.ticks);

	if (-EINVAL == err) {
		printk("Alarm settings invalid\n");
        return false;
	} else if (-ENOTSUP == err) {
		printk("Alarm setting request not supported\n");
        return false;
	} else if (err != 0) {
		printk("Error\n");
        return false;
	}
    return true;
}

bool gpio_init() {
    int err;
    gpio_device = device_get_binding(DT_GPIO_P1_DEV_NAME);
    if(gpio_device == NULL) {
        printk("GPIO device not found\n");
        return false;
    }

    err = gpio_pin_configure(gpio_device, speedometer_pin, GPIO_DIR_IN);
    if(err) {
        printk("GPIO speedometer PIN CONFIGURE ERROR: %d \n", err);
        return false;
    }

    err = gpio_pin_configure(gpio_device, led_pin, GPIO_DIR_OUT);
    if(err) {
        printk("GPIO led PIN CONFIGURE ERROR: %d \n", err);
        return false;
    }


    return true;
}

bool speedometer_init() {
    return gpio_init() && counter_init();
};
