#pragma once
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>

enum hrm_state {
    HRM_CONNECTED_CONTACT,
    HRM_CONNECTED_NO_CONTACT,
    HRM_CONNECTED,
    HRM_DISCONNECTED,
    HRM_INIT
};

struct hrm_data {
    u16_t hr;
    enum hrm_state state;
};

extern struct hrm_data hrm_data;

void hrm_connected(struct bt_conn *conn, u8_t conn_err);
void hrm_disconnected(struct bt_conn *conn, u8_t reason);
