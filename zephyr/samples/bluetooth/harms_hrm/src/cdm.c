#include "cdm.h"
#include <zephyr/types.h>
#include <stddef.h>
#include <errno.h>
#include <zephyr.h>
#include <sys/printk.h>
#include <stdio.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>
#include <sys/byteorder.h>

static struct bt_uuid_16 uuid = BT_UUID_INIT_16(0);
static struct bt_gatt_discover_params discover_params;
static struct bt_gatt_subscribe_params subscribe_params;

static uint16_t prev_crank_event_time;
static uint16_t prev_crank_revolutions;
static bool first_measurement;

struct cdm_data cdm_data = {
    .rpm = 0,
    .state = CDM_INIT,
    .cumulative_0rpm = 0
};

static u8_t notify_func(struct bt_conn *conn,
			   struct bt_gatt_subscribe_params *params,
			   const void * data, u16_t length)
{
	if (!data) {
		printk("[cdm][UNSUBSCRIBED]\n");
		params->value_handle = 0U;
		return BT_GATT_ITER_STOP;
	}

	printk("[cdm][NOTIFICATION] data %p length %u\n", data, length);

    uint8_t* data_u8 = (uint8_t*) data;

    bool wheel_revolution_data_present = data_u8[0] & 1;
    bool crank_revolution_data_present = data_u8[0] & 2;

    size_t crank_data_index = sizeof(u8_t);
    if(wheel_revolution_data_present) {
        crank_data_index = sizeof(u8_t) + sizeof(u32_t) + sizeof(u16_t);
    }

    if(crank_revolution_data_present) {
        uint16_t last_crank_event_time;
        memcpy(&last_crank_event_time, &data_u8[crank_data_index + sizeof(u16_t)], sizeof(u16_t));
        last_crank_event_time = sys_le16_to_cpu(last_crank_event_time);

        const uint16_t time_diff = last_crank_event_time - prev_crank_event_time;

        if(time_diff == 0) {
            printf("0 RPM\n");
            if(cdm_data.cumulative_0rpm != 0xffff) {
                cdm_data.cumulative_0rpm += 1;
            }
        } else {
            cdm_data.cumulative_0rpm = 0;

            uint16_t cumulative_crank_revolutions;
            memcpy(&cumulative_crank_revolutions, &data_u8[crank_data_index], sizeof(u16_t));
            cumulative_crank_revolutions = sys_le16_to_cpu(cumulative_crank_revolutions);
            const uint16_t revolutions_diff = cumulative_crank_revolutions - prev_crank_revolutions;

            prev_crank_event_time = last_crank_event_time;
            prev_crank_revolutions = cumulative_crank_revolutions;

            if(!first_measurement) {
                // In the sensor data, 1 second is 1024 units.
                // shift 1 to the left for rounding bit
                const uint32_t ONE_MINUTE_1 = (1024 * 60) << 1;
                const uint32_t revolutions_per_minute_1 = (revolutions_diff * ONE_MINUTE_1)/time_diff;
                const bool round_up = revolutions_per_minute_1 & 0x1;
                cdm_data.rpm = (revolutions_per_minute_1 >> 1) + (round_up ? 1 : 0);
                //printf("OM8: %d, rpm8: %d, ru: %d\n", ONE_MINUTE_1, revolutions_per_minute_1, round_up);
                //printf("Cumcrank: %d, lwt: %d/1024s RPM: %d\n", cumulative_crank_revolutions, last_crank_event_time, cdm_data.rpm);
                printf("%d RPM", cdm_data.rpm);
            }
            first_measurement = false;
        }

    }

	return BT_GATT_ITER_CONTINUE;
}

static u8_t discover_func(struct bt_conn *conn,
			     const struct bt_gatt_attr *attr,
			     struct bt_gatt_discover_params *params)
{
	int err;

	if (!attr) {
		printk("[cdm]Discover complete\n");
		(void)memset(params, 0, sizeof(*params));
		return BT_GATT_ITER_STOP;
	}

	printk("[cdm][ATTRIBUTE] handle %u\n", attr->handle);

	if (!bt_uuid_cmp(discover_params.uuid, BT_UUID_CSC)) {
		memcpy(&uuid, BT_UUID_CSC_MEASUREMENT, sizeof(uuid));
		discover_params.uuid = &uuid.uuid;
		discover_params.start_handle = attr->handle + 1;
		discover_params.type = BT_GATT_DISCOVER_CHARACTERISTIC;

		err = bt_gatt_discover(conn, &discover_params);
		if (err) {
			printk("[cdm]Discover failed (err 0x%02x)\n", err);
		}
	} else if (!bt_uuid_cmp(discover_params.uuid,
				BT_UUID_CSC_MEASUREMENT)) {
		memcpy(&uuid, BT_UUID_GATT_CCC, sizeof(uuid));
		discover_params.uuid = &uuid.uuid;
		discover_params.start_handle = attr->handle + 2;
		discover_params.type = BT_GATT_DISCOVER_DESCRIPTOR;
		subscribe_params.value_handle = bt_gatt_attr_value_handle(attr);

		err = bt_gatt_discover(conn, &discover_params);
		if (err) {
			printk("[cdm]Discover failed (err 0x%02x)\n", err);
		}
	} else {
		subscribe_params.notify = notify_func;
		subscribe_params.value = BT_GATT_CCC_NOTIFY;
		subscribe_params.ccc_handle = attr->handle;

		err = bt_gatt_subscribe(conn, &subscribe_params);
		if (err && err != -EALREADY) {
			printk("[cdm]Subscribe failed (err 0x%02x)\n", err);
		} else {
			printk("[cdm][SUBSCRIBED]\n");
		}

		return BT_GATT_ITER_STOP;
	}

	return BT_GATT_ITER_STOP;
}

void cdm_connected(struct bt_conn *conn, u8_t conn_err) {
	int err;
    cdm_data.state = CDM_CONNECTED;

    // Reset counters to 0
    prev_crank_event_time = 0;
    prev_crank_revolutions = 0;
    cdm_data.cumulative_0rpm = 0;
    first_measurement = true;

    memcpy(&uuid, BT_UUID_CSC, sizeof(uuid));
    discover_params.uuid = &uuid.uuid;
    discover_params.func = discover_func;
    discover_params.start_handle = 0x0001;
    discover_params.end_handle = 0xffff;
    discover_params.type = BT_GATT_DISCOVER_PRIMARY;

    err = bt_gatt_discover(conn, &discover_params);
    if (err) {
        printk("[cdm]Discover failed(err 0x%02x)\n", err);
        return;
    }
}

void cdm_disconnected(struct bt_conn *conn, u8_t reason) {
    cdm_data.state = CDM_DISCONNECTED;
}
