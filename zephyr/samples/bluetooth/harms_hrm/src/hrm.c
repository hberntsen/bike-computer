#include "hrm.h"
#include <zephyr/types.h>
#include <stddef.h>
#include <errno.h>
#include <zephyr.h>
#include <sys/printk.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>
#include <sys/byteorder.h>

struct hrm_data hrm_data = {
    .hr = 0,
    .state = HRM_INIT
};

static struct bt_uuid_16 uuid = BT_UUID_INIT_16(0);
static struct bt_gatt_discover_params discover_params;
static struct bt_gatt_subscribe_params subscribe_params;

static u8_t notify_func(struct bt_conn *conn,
			   struct bt_gatt_subscribe_params *params,
			   const void * data, u16_t length)
{
	if (!data) {
		printk("[hrm][UNSUBSCRIBED]\n");
		params->value_handle = 0U;
		return BT_GATT_ITER_STOP;
	}

	printk("[hrm][NOTIFICATION] data %p length %u\n", data, length);

    uint8_t* data_u8 = (uint8_t*) data;

    // True when contact not supported or cannot be detected
    bool contact = ((data_u8[0] >> 1) & 3) != 2;
    if(contact) {
        uint16_t hr = 0;
        // heart rate value format bit
        if(data_u8[0] & 1) {
            hr = *((uint16_t*)&data_u8[1]);
        } else {
            hr = data_u8[1];
        }
        hrm_data.state = HRM_CONNECTED_CONTACT;
        if(hrm_data.hr != hr) {
            hrm_data.hr = hr;
            printk("[hrm]Contact, %d bpm\n", hr);
        }
    } else {
        if(hrm_data.state != HRM_CONNECTED_NO_CONTACT) {
            hrm_data.state = HRM_CONNECTED_NO_CONTACT;
            printk("[hrm]No contact\n");
        }
    }

	return BT_GATT_ITER_CONTINUE;
}

static u8_t discover_func(struct bt_conn *conn,
			     const struct bt_gatt_attr *attr,
			     struct bt_gatt_discover_params *params)
{
	int err;

	if (!attr) {
		printk("[hrm]Discover complete\n");
		(void)memset(params, 0, sizeof(*params));
		return BT_GATT_ITER_STOP;
	}

	printk("[hrm][ATTRIBUTE] handle %u\n", attr->handle);

	if (!bt_uuid_cmp(discover_params.uuid, BT_UUID_HRS)) {
		memcpy(&uuid, BT_UUID_HRS_MEASUREMENT, sizeof(uuid));
		discover_params.uuid = &uuid.uuid;
		discover_params.start_handle = attr->handle + 1;
		discover_params.type = BT_GATT_DISCOVER_CHARACTERISTIC;

		err = bt_gatt_discover(conn, &discover_params);
		if (err) {
			printk("[hrm]Discover failed (err 0x%02x)\n", err);
		}
	} else if (!bt_uuid_cmp(discover_params.uuid,
				BT_UUID_HRS_MEASUREMENT)) {
		memcpy(&uuid, BT_UUID_GATT_CCC, sizeof(uuid));
		discover_params.uuid = &uuid.uuid;
		discover_params.start_handle = attr->handle + 2;
		discover_params.type = BT_GATT_DISCOVER_DESCRIPTOR;
		subscribe_params.value_handle = bt_gatt_attr_value_handle(attr);

		err = bt_gatt_discover(conn, &discover_params);
		if (err) {
			printk("[hrm]Discover failed (err 0x%02x)\n", err);
		}
	} else {
		subscribe_params.notify = notify_func;
		subscribe_params.value = BT_GATT_CCC_NOTIFY;
		subscribe_params.ccc_handle = attr->handle;

		err = bt_gatt_subscribe(conn, &subscribe_params);
		if (err && err != -EALREADY) {
			printk("[hrm]Subscribe failed (err 0x%02x)\n", err);
		} else {
			printk("[hrm][SUBSCRIBED]\n");
		}

		return BT_GATT_ITER_STOP;
	}

	return BT_GATT_ITER_STOP;
}

void hrm_connected(struct bt_conn *conn, u8_t conn_err) {
	int err;
    hrm_data.state = HRM_CONNECTED;
    memcpy(&uuid, BT_UUID_HRS, sizeof(uuid));
    discover_params.uuid = &uuid.uuid;
    discover_params.func = discover_func;
    discover_params.start_handle = 0x0001;
    discover_params.end_handle = 0xffff;
    discover_params.type = BT_GATT_DISCOVER_PRIMARY;

    err = bt_gatt_discover(conn, &discover_params);
    if (err) {
        printk("[hrm]Discover failed(err 0x%02x)\n", err);
        return;
    }
}

void hrm_disconnected(struct bt_conn *conn, u8_t reason) {
    hrm_data.state = HRM_DISCONNECTED;
}
