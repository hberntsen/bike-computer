#pragma once
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>

enum cdm_state {
    CDM_CONNECTED,
    CDM_DISCONNECTED,
    CDM_INIT
};

struct cdm_data {
    u16_t rpm;
    enum cdm_state state;
    /** 
    * The number of consecuetive times we received a notification with 0 RPM. The
    * Wahoo Cadence sensor sends an event approx every second so we can use this
    * for a rough inactivity indication.
    */
    uint16_t cumulative_0rpm;
};

extern struct cdm_data cdm_data;

void cdm_connected(struct bt_conn *conn, u8_t conn_err);
void cdm_disconnected(struct bt_conn *conn, u8_t reason);
