#include "temperature.h"
#include <drivers/sensor.h>

static struct device *temperature_device;

void temperature_init() {
	temperature_device = device_get_binding("TEMP_0");
    if(temperature_device == NULL) {
        printk("Temperature device not found\n");
    }
}

int temperature_get(double* out) {
    struct sensor_value temp_value;

    if(!temperature_device) {
        return -1;
    }

    int err = sensor_sample_fetch(temperature_device);
    if(err) {
        printk("sensor_sample_fetch failed, return: %d\n", err);
        return -2;
    }

    err = sensor_channel_get(temperature_device, SENSOR_CHAN_DIE_TEMP, &temp_value);
    if(err) {
        printk("sensor_channel_get failed return: %d\n", err);
        return -3;
    }

    *out = sensor_value_to_double(&temp_value);

    return 0;
}
