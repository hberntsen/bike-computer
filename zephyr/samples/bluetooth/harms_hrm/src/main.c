/* main.c - Application main entry point */

/*
 * Copyright (c) 2015-2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/types.h>
#include <stddef.h>
#include <errno.h>
#include <zephyr.h>
#include <sys/printk.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>
#include <sys/byteorder.h>

#include "state.h"
#include "display.h"
#include "hrm.h"
#include "cdm.h"
#include "temperature.h"

static struct bt_conn *hrm_conn;
static struct bt_conn *cdm_conn;


#define BT_LE_SCAN_PASSIVE_SLOW BT_LE_SCAN_PASSIVE
#if 0
#define BT_LE_SCAN_PASSIVE_SLOW BT_LE_SCAN_PARAM(BT_HCI_LE_SCAN_PASSIVE, \
					    BT_HCI_LE_SCAN_FILTER_DUP_ENABLE, \
					    BT_GAP_SCAN_SLOW_INTERVAL_1, \
					    BT_GAP_SCAN_SLOW_WINDOW_1)
#endif


static bool eir_found(struct bt_data *data, void *user_data)
{
	bt_addr_le_t *addr = user_data;
	int i;

	printk("[AD]: %u data_len %u\n", data->type, data->data_len);

	switch (data->type) {
	case BT_DATA_UUID16_SOME:
	case BT_DATA_UUID16_ALL:
		if (data->data_len % sizeof(u16_t) != 0U) {
			printk("AD malformed\n");
			return true;
		}

		for (i = 0; i < data->data_len; i += sizeof(u16_t)) {
			struct bt_uuid *uuid;
			u16_t u16;
			int err;

			memcpy(&u16, &data->data[i], sizeof(u16));
			uuid = BT_UUID_DECLARE_16(sys_le16_to_cpu(u16));
            bool has_hrs = !bt_uuid_cmp(uuid, BT_UUID_HRS);
            bool has_csc = !bt_uuid_cmp(uuid, BT_UUID_CSC);
            if(has_hrs) {
                printk("hrs found\n");
            }
            if(has_csc) {
                printk("csc found\n");
            }

            if(has_hrs && hrm_conn == NULL) {
                err = bt_le_scan_stop();
                if (err) {
                    printk("Stop LE scan failed (err 0x%02x)\n", err);
                    continue;
                }
                //TODO: inspect conn params
                hrm_conn = bt_conn_create_le(addr,
                                BT_LE_CONN_PARAM_DEFAULT);
                return false;
            } else if(has_csc && cdm_conn == NULL) {
                err = bt_le_scan_stop();
                if (err) {
                    printk("Stop LE scan failed (err 0x%02x)\n", err);
                    continue;
                }
                //TODO: inspect conn params
                cdm_conn = bt_conn_create_le(addr,
                                BT_LE_CONN_PARAM_DEFAULT);
                return false;
            }
		}
	}

	return true;
}

static void device_found(const bt_addr_le_t *addr, s8_t rssi, u8_t type,
			 struct net_buf_simple *ad)
{
	char dev[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(addr, dev, sizeof(dev));
	printk("[DEVICE]: %s, AD evt type %u, AD data len %u, RSSI %i\n",
	       dev, type, ad->len, rssi);

	/* We're only interested in connectable events */
	if (type == BT_LE_ADV_IND || type == BT_LE_ADV_DIRECT_IND) {
		bt_data_parse(ad, eir_found, (void *)addr);
	}
}

static void connected(struct bt_conn *conn, u8_t conn_err)
{
	char addr[BT_ADDR_LE_STR_LEN];
	int err;

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	if (conn_err) {
		printk("Failed to connect to %s (0x%02x)\n", addr, conn_err);
        if(conn == hrm_conn) {
            printk("hrm_conn = NULL\n");
            hrm_conn = NULL;
        } else if (conn == cdm_conn) {
            printk("cdm_conn = NULL\n");
            cdm_conn = NULL;
        }
	} else {
        printk("Connected: %s\n", addr);

        if (conn == hrm_conn) {
            hrm_connected(conn, conn_err);
        } else if (conn == cdm_conn) {
            cdm_connected(conn, conn_err);
        }
    }

    if(!hrm_conn || !cdm_conn) {
        printk("Resume scanning...\n");
        err = bt_le_scan_start(BT_LE_SCAN_PASSIVE_SLOW, device_found);
        if (err && err != -EALREADY) {
            printk("Scanning failed to start (err %d)\n", err);
        }
    }
}

static void disconnected(struct bt_conn *conn, u8_t reason)
{
	char addr[BT_ADDR_LE_STR_LEN];
	int err;

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	printk("Disconnected: %s (reason %u)\n", addr, reason);

	if (hrm_conn == conn) {
        hrm_disconnected(conn, reason);

        bt_conn_unref(hrm_conn);
        hrm_conn = NULL;
	} else if (cdm_conn == conn) {
        cdm_disconnected(conn, reason);

        bt_conn_unref(cdm_conn);
        cdm_conn = NULL;
	}
    err = bt_le_scan_start(BT_LE_SCAN_PASSIVE_SLOW, device_found);
    if (err && err != -EALREADY) {
        printk("Scanning failed to start (err %d)\n", err);
    }
}

static struct bt_conn_cb conn_callbacks = {
	.connected = connected,
	.disconnected = disconnected,
};

void main(void)
{
	int err;
    hrm_conn = NULL;
    cdm_conn = NULL;

	err = bt_enable(NULL);

	if (err) {
		printk("Bluetooth init failed (err 0x%02x)\n", err);
		return;
	}

	printk("Bluetooth initialized\n");

	bt_conn_cb_register(&conn_callbacks);

	err = bt_le_scan_start(BT_LE_SCAN_PASSIVE_SLOW, device_found);

	if (err) {
		printk("Scanning failed to start (err 0x%02x)\n", err);
		return;
	}

	printk("Scanning successfully started\n");

    temperature_init();

}
