#include <drivers/display.h>
#include <display/pcd8544.h>
#include "u8g2/u8g2.h"
#include <stdio.h>
#include "temperature.h"
#include "hrm.h"
#include "cdm.h"


static struct device *display_device;
static u8_t* buffer1;
static u8_t buffer2[88*5];

static bool hrm_dirty = false;
static bool cdm_dirty = false;

#define HRM_WIDTH 36
#define CDM_START 51
#define CDM_WIDTH (84-CDM_START)

static u8g2_t u8g2;

#define DISPLAY_THREAD_STACK_SIZE 500
#define DISPLAY_SLEEP_TIME 1000
/// Second lowest priority
#define DISPLAY_THREAD_PRIORITY (CONFIG_NUM_PREEMPT_PRIORITIES - 2)

void display_thread_entry_point(void *, void *, void *);

K_THREAD_STACK_DEFINE(display_stack_area, DISPLAY_THREAD_STACK_SIZE);
struct k_thread display_thread_data;

K_THREAD_DEFINE(display_tid, DISPLAY_THREAD_STACK_SIZE,
        display_thread_entry_point, NULL, NULL, NULL,
        DISPLAY_THREAD_PRIORITY, 0, K_NO_WAIT);
        //DISPLAY_THREAD_PRIORITY, K_USER, K_NO_WAIT);

static void render_static() {
  u8g2_SetFont(&u8g2, u8g2_font_blipfest_07_tr);
  u8g2_DrawStr(&u8g2, 36, 20, "bpm");
  u8g2_DrawStr(&u8g2, 38, 11, "rpm");

  // bpm/cdm separation box
  u8g2_DrawVLine(&u8g2, HRM_WIDTH, 0, 13);
  u8g2_DrawHLine(&u8g2, HRM_WIDTH, 13, 15);
  u8g2_DrawVLine(&u8g2, 50, 14, 8);
  u8g2_DrawHLine(&u8g2, 0, 22, 84);

  // status bar
  u8g2_SetDrawColor(&u8g2, 1);
  u8g2_DrawBox(&u8g2, 0, 42, 84, 6);
}

/*
static void draw_digit(u8g2_uint_t x, u8g2_uint_t y, uint16_t glyph, struct digit* digit) {
    if(glyph != digit->last) {
        digit->last = glyph;
        u8g2_SetClipWindow(digit->region.start, 0, digit->region.width);
        u8g2_DrawGlyph(&u8g2, x, y, glyph);
        digit->region.tainted = 1;
    }
}
*/

static void render_hrm() {
    static struct hrm_data previous_data;

    if(memcmp(&previous_data, &hrm_data, sizeof(struct hrm_data)) == 0) {
        hrm_dirty = false;
        return;
    } else {
        hrm_dirty = true;
        memcpy(&previous_data, &hrm_data, sizeof(struct hrm_data));
    }

    u8g2_SetClipWindow(&u8g2, 0, 0, HRM_WIDTH, 22);
    u8g2_SetDrawColor(&u8g2, 0);
    u8g2_DrawBox(&u8g2, 0 , 0, HRM_WIDTH, 22);
    u8g2_SetDrawColor(&u8g2, 1);

    u8g2_SetFont(&u8g2, u8g2_font_logisoso20_tr);

    if(hrm_data.state == HRM_CONNECTED_CONTACT) {
        const uint16_t hrm = hrm_data.hr;

        if(hrm <= 999) {
            uint8_t buf[4] = {0};
            uint8_t buf_start = 2;

            buf[2] = '0' + (hrm % 10);
            if(hrm >= 10) {
                buf[1] = '0' + ((hrm / 10) % 10);
                buf_start = 1;
                if(hrm >= 100) {
                    buf[0] = '0' + (hrm / 100);
                    buf_start = 0;
                }
            };

            u8g2_uint_t width = u8g2_GetStrWidth(&u8g2, buf + buf_start);
            u8g2_DrawStr(&u8g2, HRM_WIDTH - 1 - width, 21, buf + buf_start);
        }
    } else if(hrm_data.state == HRM_CONNECTED_NO_CONTACT) {
        u8g2_uint_t width = u8g2_GetStrWidth(&u8g2, "NC");
        u8g2_DrawStr(&u8g2, HRM_WIDTH - 1 - width, 21, "NC");
    } else if(hrm_data.state == HRM_CONNECTED) {
        u8g2_uint_t width = u8g2_GetStrWidth(&u8g2, "Con");
        u8g2_DrawStr(&u8g2, HRM_WIDTH - 1 - width, 21, "Con");
    } else if(hrm_data.state == HRM_DISCONNECTED) {
        u8g2_uint_t width = u8g2_GetStrWidth(&u8g2, "Dis");
        u8g2_DrawStr(&u8g2, HRM_WIDTH - 1 - width, 21, "Dis");
    }
}


static void render_cdm() {
    static uint16_t previous_rpm;
    static enum cdm_state previous_state;

    uint16_t rpm = cdm_data.cumulative_0rpm > 4 ? 0 : cdm_data.rpm;

    if(rpm == previous_rpm && previous_state == cdm_data.state) {
        cdm_dirty = false;
    } else {
        cdm_dirty = true;
        previous_rpm = rpm;
        previous_state = cdm_data.state;
    }

    u8g2_SetClipWindow(&u8g2, CDM_START, 0, CDM_START + CDM_WIDTH, 22);
    u8g2_SetDrawColor(&u8g2, 0);
    u8g2_DrawBox(&u8g2, CDM_START, 0, CDM_WIDTH, 22);
    u8g2_SetDrawColor(&u8g2, 1);

    u8g2_SetFont(&u8g2, u8g2_font_logisoso20_tr);
    printf("Rendering new cdm\n");

    if(cdm_data.state == CDM_CONNECTED) {
        if(rpm <= 999) {
            uint8_t buf[4] = {0};
            uint8_t buf_start = 2;

            buf[2] = '0' + (rpm % 10);
            if(rpm >= 10) {
                buf[1] = '0' + ((rpm / 10) % 10);
                buf_start = 1;
                if(rpm >= 100) {
                    buf[0] = '0' + (rpm / 100);
                    buf_start = 0;
                }
            };

            u8g2_uint_t width = u8g2_GetStrWidth(&u8g2, buf + buf_start);
            printf("Rendering cdm with width %d, start %d and str %s\n", width, CDM_START + (CDM_WIDTH - width), buf+buf_start);
            u8g2_DrawStr(&u8g2, CDM_START + (CDM_WIDTH - width), 21, buf + buf_start);
        }
    }  else if(cdm_data.state == CDM_DISCONNECTED) {
        u8g2_uint_t width = u8g2_GetStrWidth(&u8g2, "Dis");
        u8g2_DrawStr(&u8g2, CDM_START + (CDM_WIDTH - width), 21, "Dis");
    }
}


void display_thread_entry_point(void *_1_, void * _2, void * _3) {
	display_device = device_get_binding("PCD8544");

	if(display_device == NULL) {
		printk("Display device not found\n");
		return;
	} else { 
		printk("Display initialised\n");
    }

    u8g2_Setup_pcd8544_84x48_f(&u8g2, U8G2_R0, u8x8_byte_empty, u8x8_dummy_cb);
    //buffer1 = u8g2.tile_buf_ptr;
    //render_static();
    //u8g2.tile_buf_ptr = buffer2;
    render_static();

    pcd8544_write_u8g2(display_device, u8g2.tile_buf_ptr);

    for(;;) {
        printk("Writing to screen\n");
        render_hrm();
        render_cdm();
        if(hrm_dirty || cdm_dirty) {
            pcd8544_write_u8g2(display_device, u8g2.tile_buf_ptr);
        }

        k_sleep(DISPLAY_SLEEP_TIME);
    }
}
