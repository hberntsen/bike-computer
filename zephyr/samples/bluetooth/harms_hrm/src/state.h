#pragma once

#include <zephyr.h>

extern struct k_sem state_updated;

enum state_hr_connection {
    STATE_HR_CONNECTED_CONTACT,
    STATE_HR_CONNECTED_NO_CONTACT,
    STATE_HR_CONNECTED,
    STATE_HR_DISCONNECTED,
    STATE_HR_INITIAL
};

struct state {
    u16_t hr;
    enum state_hr_connection hr_connection;
};

extern struct state state;

