#include "state.h"

K_SEM_DEFINE(state_updated, 1, 1)

struct state state = {
    .hr = 0,
    .hr_connection = STATE_HR_DISCONNECTED
};
