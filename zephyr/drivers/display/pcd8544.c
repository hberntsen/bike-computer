#define LOG_LEVEL CONFIG_DISPLAY_LOG_LEVEL
#include <logging/log.h>
LOG_MODULE_REGISTER(pcd8544);

#include <string.h>
#include <device.h>
#include <drivers/display.h>
#include <init.h>
#include <drivers/gpio.h>
#include <drivers/spi.h>
#include <sys/byteorder.h>

#include <display/pcd8544.h>

/**
 * PCD8544 controller driver.
 */

#define PCD8544_SPI_FREQ DT_INST_0_PHILIPS_PCD8544FB_SPI_MAX_FREQUENCY
#define PCD8544_BUS_NAME DT_INST_0_PHILIPS_PCD8544FB_BUS_NAME
#define PCD8544_DC_PIN DT_INST_0_PHILIPS_PCD8544FB_DC_GPIOS_PIN
#define PCD8544_DC_CNTRL DT_INST_0_PHILIPS_PCD8544FB_DC_GPIOS_CONTROLLER
#define PCD8544_CS_PIN DT_INST_0_PHILIPS_PCD8544FB_CS_GPIOS_PIN
#define PCD8544_CS_CNTRL DT_INST_0_PHILIPS_PCD8544FB_CS_GPIOS_CONTROLLER
#define PCD8544_RESET_PIN DT_INST_0_PHILIPS_PCD8544FB_RESET_GPIOS_PIN
#define PCD8544_RESET_CNTRL DT_INST_0_PHILIPS_PCD8544FB_RESET_GPIOS_CONTROLLER

#define PCD8544_PANEL_WIDTH		84
#define PCD8544_PANEL_HEIGHT		48
#define PCD8544_PANEL_NUMOF_COLUMS		PCD8544_PANEL_WIDTH
#define PCD8544_PANEL_NUMOF_ROWS_PER_PAGE	8
#define PCD8544_PANEL_NUMOF_PAGES		(PCD8544_PANEL_HEIGHT / \
					 PCD8544_PANEL_NUMOF_ROWS_PER_PAGE)

#define PCD8544_PANEL_FIRST_PAGE	0
#define PCD8544_PANEL_LAST_PAGE		(PCD8544_PANEL_NUMOF_PAGES - 1)
#define PCD8544_PANEL_FIRST_GATE	0
#define PCD8544_PANEL_LAST_GATE		(PCD8544_PANEL_NUMOF_COLUMS - 1)

#define PCD8544_PIXELS_PER_BYTE		8

// Commands
#define PCD8544_CMD_SET_X	0x80
#define PCD8544_CMD_SET_Y	0x40
//todo: cmd prefix
#define PCD8544_EXTENDEDMODE	0x21 // H=0
#define PCD8544_NORMALMODE	0x20 // H=1
#define PCD8544_CMD_POWER_DOWN  0x24
#define PCD8544_CMD_POWER_UP  0x20
#define PCD8544_CMD_VOP	0x80
#define PCD8544_CMD_BIAS	0x10
#define PCD8544_CMD_DISPLAYCONTROL	0x08
#define PCD8544_CMD_DISPLAY_NORMAL	0x04
#define PCD8544_CMD_DISPLAY_BLANK	0x00
#define PCD8544_CMD_DISPLAY_ALL_ON	0x01
#define PCD8544_CMD_DISPLAY_INVERTED	0x05

#define PCD8544_RESET_DELAY 1 //todo: this is ms, hw allows this to be 100 ns
#define PCD8544_BIAS 4 // datasheets specifies this as best value
#define PCD8544_DEFAULT_VOP 64


struct pcd8544_data {
	struct device *reset;
	struct device *dc;
	struct device *spi_dev;
	struct spi_config spi_config;
	struct spi_cs_control cs_ctrl;
    /// The current x address in the display's internal register
    u8_t x;
    /// The current y address in the display's internal register
    u8_t y;
};

static inline int pcd8544_write_cmd(struct pcd8544_data *driver, u8_t cmd)
{
	struct spi_buf buf = {.buf = &cmd, .len = sizeof(cmd)};
	struct spi_buf_set buf_set = {.buffers = &buf, .count = 1};

	gpio_pin_write(driver->dc, PCD8544_DC_PIN, 0);
	return spi_write(driver->spi_dev, &driver->spi_config, &buf_set);
}

static inline int pcd8544_write_data(struct pcd8544_data *driver, u8_t *data, size_t len)
{
	struct spi_buf buf = {.buf = data, .len = len};
	struct spi_buf_set buf_set = {.buffers = &buf, .count = 1};

	gpio_pin_write(driver->dc, PCD8544_DC_PIN, 1);
	return spi_write(driver->spi_dev, &driver->spi_config, &buf_set);
}

static int pcd8544_blanking_off(const struct device *dev)
{
	struct pcd8544_data *driver = dev->driver_data;
	int err = pcd8544_write_cmd(driver, PCD8544_CMD_POWER_UP);
	if (err < 0) {
		LOG_ERR("PCD8544 Set POWER_UP error");
		return err;
	}
	///return -ENOTSUP;
	return 0;
}

static int pcd8544_blanking_on(const struct device *dev)
{
	//TODO: zero display RAM since the datasheet says that saves power
	struct pcd8544_data *driver = dev->driver_data;
	int err = pcd8544_write_cmd(driver, PCD8544_CMD_POWER_DOWN);
	if (err < 0) {
		LOG_ERR("PCD8544 Set POWER_DOWN error");
		return err;
	}

	return 0;
}

static int pcd8544_write(const struct device *dev, const u16_t x,
			 const u16_t y,
			 const struct display_buffer_descriptor *desc,
			 const void *buf)
{
	struct pcd8544_data *driver = dev->driver_data;
	int err;

    // TODO: figure out what pitch means
	if (desc->pitch != PCD8544_PANEL_WIDTH) {
		LOG_ERR("Pitch is not equal to width");
		return -EINVAL;
	}

	if (buf == NULL || desc->buf_size == 0U) {
		LOG_ERR("Display buffer is not available");
		return -EINVAL;
	}

    if(y & 7) {
		LOG_ERR("Y must be multiple of 8");
		return -EINVAL;
    }
	if ((y + desc->height) > PCD8544_PANEL_HEIGHT) {
		LOG_ERR("Buffer out of bounds (height)");
		return -EINVAL;
	}

	if ((x + desc->width) > PCD8544_PANEL_WIDTH) {
		LOG_ERR("Buffer out of bounds (width)");
		return -EINVAL;
	}

	if (x != 0U && y != 0U) {
		LOG_ERR("Unsupported origin");
		return -1;
	}

	err = pcd8544_write_cmd(driver, PCD8544_CMD_SET_X | x);
	if (err < 0) {
		LOG_ERR("PCD8544 Set X error");
		return err;
	}

	err = pcd8544_write_cmd(driver, PCD8544_CMD_SET_Y | y);
	if (err < 0) {
		LOG_ERR("PCD8544 Set Y error");
		return err;
	}

	return pcd8544_write_data(driver, (u8_t *)buf, desc->buf_size);
}

int pcd8544_write_from(const struct device *dev, const u16_t x, const u16_t row, const u16_t len, unsigned char *buf) {
    int err;
	struct pcd8544_data *driver = dev->driver_data;

    if(driver->x != x) {
        if(x > 83) {
            return -EINVAL;
        }
        err = pcd8544_write_cmd(driver, PCD8544_CMD_SET_X | x);
        if (err < 0) {
            LOG_ERR("PCD8544 Set X error");
            return err;
        }
    }
    if(driver->y != row) {
        if(x > 5) {
            return -EINVAL;
        }
        err = pcd8544_write_cmd(driver, PCD8544_CMD_SET_Y | row);
        if (err < 0) {
            LOG_ERR("PCD8544 Set Y error");
            return err;
        }
    }
    driver->y += (x + len) / PCD8544_PANEL_WIDTH;
    driver->x += (x + len) % 84;
	return pcd8544_write_data(driver, (u8_t *)buf, len);
}

int pcd8544_write_u8g2(const struct device *dev, unsigned char *buf)
{
	struct pcd8544_data *driver = dev->driver_data;
	int err;

    driver->x = 0;
	err = pcd8544_write_cmd(driver, PCD8544_CMD_SET_X | 0);
	if (err < 0) {
		LOG_ERR("PCD8544 Set X error");
		return err;
	}

    driver->y = 0;
	err = pcd8544_write_cmd(driver, PCD8544_CMD_SET_Y | 0);
	if (err < 0) {
		LOG_ERR("PCD8544 Set Y error");
		return err;
	}

	const struct spi_buf pages[PCD8544_PANEL_NUMOF_PAGES] = {
		{
			.buf = buf,
			.len = PCD8544_PANEL_WIDTH,
		},
		{
			.buf = buf + 88,
			.len = PCD8544_PANEL_WIDTH,
		},
		{
			.buf = buf + 88*2,
			.len = PCD8544_PANEL_WIDTH,
		},
		{
			.buf = buf + 88*3,
			.len = PCD8544_PANEL_WIDTH,
		},
		{
			.buf = buf + 88*4,
			.len = PCD8544_PANEL_WIDTH,
		},
		{
			.buf = buf + 88*5,
			.len = PCD8544_PANEL_WIDTH,
		}
	};

	struct spi_buf_set buf_set = {.buffers = pages, .count = PCD8544_PANEL_NUMOF_PAGES};
	gpio_pin_write(driver->dc, PCD8544_DC_PIN, 1);
	return spi_write(driver->spi_dev, &driver->spi_config, &buf_set);
}


static int pcd8544_read(const struct device *dev, const u16_t x,
			const u16_t y,
			const struct display_buffer_descriptor *desc,
			void *buf)
{
	LOG_ERR("not supported");
	return -ENOTSUP;
}

static void *pcd8544_get_framebuffer(const struct device *dev)
{
	LOG_ERR("not supported");
	return NULL;
}

static int pcd8544_set_brightness(const struct device *dev,
				  const u8_t brightness)
{
	LOG_WRN("not supported");
	return -ENOTSUP;
}

static int pcd8544_set_contrast(const struct device *dev, u8_t contrast)
{
	struct pcd8544_data *driver = dev->driver_data;
	int err = pcd8544_write_cmd(driver, PCD8544_EXTENDEDMODE);
	if (err < 0) {
		LOG_ERR("PCD8544 Set PCD8544_EXTENDEDMODE error");
		return err;
	}

	if(contrast > 0x7f) {
		contrast = 0x7f;
	}

	err = pcd8544_write_cmd(driver, PCD8544_CMD_VOP | contrast);
	if (err < 0) {
		LOG_ERR("PCD8544 Set contrast error");
		return err;
	}

	err = pcd8544_write_cmd(driver, PCD8544_NORMALMODE);
	if (err < 0) {
		LOG_ERR("PCD8544 Set normal mode error");
		return err;
	}

	return 0;
}


static void pcd8544_get_capabilities(const struct device *dev,
				     struct display_capabilities *caps)
{
	memset(caps, 0, sizeof(struct display_capabilities));
	caps->x_resolution = PCD8544_PANEL_WIDTH;
	caps->y_resolution = PCD8544_PANEL_HEIGHT;
	caps->supported_pixel_formats = PIXEL_FORMAT_MONO01; // todo: we can support the opposite too via inverse video mode
	caps->current_pixel_format = PIXEL_FORMAT_MONO01;
	caps->screen_info = SCREEN_INFO_MONO_VTILED;
}

static int pcd8544_set_orientation(const struct device *dev,
				   const enum display_orientation
				   orientation)
{
	LOG_ERR("Unimplemented");
	return -ENOTSUP;
}

static int pcd8544_set_pixel_format(const struct device *dev,
				    const enum display_pixel_format pf)
{
	if (pf == PIXEL_FORMAT_MONO10) {
		return 0;
	}

	LOG_ERR("not supported");
	return -ENOTSUP;
}

static int pcd8544_controller_init(struct device *dev)
{
	int err;
	struct pcd8544_data *driver = dev->driver_data;

	gpio_pin_write(driver->reset, PCD8544_RESET_PIN, 0);
	k_sleep(PCD8544_RESET_DELAY);
	gpio_pin_write(driver->reset, PCD8544_RESET_PIN, 1);
	k_sleep(PCD8544_RESET_DELAY);

	err = pcd8544_write_cmd(driver, PCD8544_EXTENDEDMODE);
	if (err < 0) {
		return err;
	}

	err = pcd8544_write_cmd(driver, PCD8544_CMD_BIAS | PCD8544_BIAS);
	if (err < 0) {
		return err;
	}

	err = pcd8544_write_cmd(driver, PCD8544_CMD_VOP | PCD8544_DEFAULT_VOP);
	if (err < 0) {
		return err;
	}

	err = pcd8544_write_cmd(driver, PCD8544_NORMALMODE);
	if (err < 0) {
		return err;
	}

	err = pcd8544_write_cmd(driver, PCD8544_CMD_DISPLAYCONTROL | PCD8544_CMD_DISPLAY_NORMAL);
	if (err < 0) {
		return err;
	}

	return 0;
}

static int pcd8544_init(struct device *dev)
{
	struct pcd8544_data *driver = dev->driver_data;

	LOG_DBG("");

	driver->spi_dev = device_get_binding(PCD8544_BUS_NAME);
	if (driver->spi_dev == NULL) {
		LOG_ERR("Could not get SPI device for PCD8544");
		return -EIO;
	}

	// todo: check frequencies
	driver->spi_config.frequency = PCD8544_SPI_FREQ;
	driver->spi_config.operation = SPI_OP_MODE_MASTER | SPI_TRANSFER_MSB | SPI_WORD_SET(8);
	driver->spi_config.slave = DT_INST_0_PHILIPS_PCD8544FB_BASE_ADDRESS;
	driver->spi_config.cs = NULL;

	driver->reset = device_get_binding(PCD8544_RESET_CNTRL);
	if (driver->reset == NULL) {
		LOG_ERR("Could not get GPIO port for PCD8544 reset");
		return -EIO;
	}

	gpio_pin_configure(driver->reset, PCD8544_RESET_PIN,
			   GPIO_DIR_OUT);

	driver->dc = device_get_binding(PCD8544_DC_CNTRL);
	if (driver->dc == NULL) {
		LOG_ERR("Could not get GPIO port for PCD8544 DC signal");
		return -EIO;
	}

	gpio_pin_configure(driver->dc, PCD8544_DC_PIN,
			   GPIO_DIR_OUT);

// todo: in my case this should be true
//#if defined(PCD8544_CS_CNTRL)
	driver->cs_ctrl.gpio_dev = device_get_binding(PCD8544_CS_CNTRL);
	if (!driver->cs_ctrl.gpio_dev) {
		LOG_ERR("Unable to get SPI GPIO CS device");
		return -EIO;
	}

	driver->cs_ctrl.gpio_pin = PCD8544_CS_PIN;
	driver->cs_ctrl.delay = 0U;
	driver->spi_config.cs = &driver->cs_ctrl;
//#endif

	return pcd8544_controller_init(dev);
}


static struct pcd8544_data pcd8544_driver;

static struct display_driver_api pcd8544_driver_api = {
	.blanking_on = pcd8544_blanking_on,
	.blanking_off = pcd8544_blanking_off,
	.write = pcd8544_write,
	.read = pcd8544_read,
	.get_framebuffer = pcd8544_get_framebuffer,
	.set_brightness = pcd8544_set_brightness,
	.set_contrast = pcd8544_set_contrast,
	.get_capabilities = pcd8544_get_capabilities,
	.set_pixel_format = pcd8544_set_pixel_format,
	.set_orientation = pcd8544_set_orientation,
};


DEVICE_AND_API_INIT(pcd8544, DT_INST_0_PHILIPS_PCD8544FB_LABEL, pcd8544_init,
		    &pcd8544_driver, NULL,
		    POST_KERNEL, CONFIG_APPLICATION_INIT_PRIORITY,
		    &pcd8544_driver_api);
